# Analyse des articles et données publiées de l'UMR TETIS

L'objectif de ce dépôt logiciel est de suivre la production (bibliographique, données, logiciel) de TETIS via les API `Hal` et `Recherche Data Gouv` pour l'UMR TETIS

**Lire le rapport** : 

- [Rapport Hal](https://hal.inrae.fr/hal-04428092v2)

**Présentations** :

- Au Data Coffe de l'Université de Strasbourg le [30 septembre 2024](https://scienceouverte.unistra.fr/agenda/evenement/data-coffee-30-09-2024)

## HAL
### Visualisation
#### Histogramme des articles/communications publiées de TETIS
Voici un histogramme des publications TETIS. 
Cependant, attention, en chaque début d'année, il y a un pic de publication. Ce pic est artificiel puisqu'il comptabilise les articles dont les informations des mois et jours sont manquant depuis leur bibtex.
![histogram_publication_tetis.png](readme.ressources/histogram_publication_tetis.png)

#### Histogramme des data ou software papers publiés par TETIS
1. Avec une liste de journaux publiant des datapers
    Le CIRAD met à disposition une liste de journaux de type data / software papers sur [ou-publier.cirad.fr](https://ou-publier.cirad.fr/revues?f%5B0%5D=types_d_articles%3AData%20papers&f%5B1%5D=types_d_articles%3ASoftware%20papers).
    Avec une fusion d'un fichier export de ce site avec un export de l'API Hal, il est alors possible de catégoriser les data ou software papers.
    Cependant, des journaux publient également des articles classiques
2. Avec le champs `researchData_s` de Hal
    | Champ | Description |
    | --- | --- |
    | `researchData_s` | Identifiants (doi) des données de recherche associées |

    **Problème**: 
    
        1. l'API dataverse CIRAD ne renvoient pas les metrics de réutilisation
        2. C'est une métadonnée qui n'est pas toujours remplie par les déposants
        3. Certains articles ne sont pas de data papers mais des articles classiques

3. À partir du champs `docSubType_s`

![histogram_datapapers_tetis](readme.ressources/histogram_datapapers_tetis.png)

![pie_data_papers](readme.ressources/pie_data_papers.png)
## Dataverse
### Ajouter ses clés API
1. Copier le fichier défaut contenant les credentials
```
cp credentials.ini.default credentials.ini
```
2. Obtenir sa clé API depuis un dataverse. Exemple ici avec Recherche.Data.Gouv:
![](./rdg_api_key.png)

### Visualisation
#### Histogramme sous forme de série temporelle de publications de jeux de données
![image0001](readme.ressources/image0001.png)
#### TOP 7 des jeux de données les plus consultées
![image0002](readme.ressources/image0002.png)
#### TOP 7 des jeux de données les plus téléchargés
![image0003](readme.ressources/image0003.png)

## Changelog
- 2022-09 : initialisation du dépôt logiciel. Dans le cadre des journées d'UMR TETIS du 06 au 08 septembre 2022.
- 2023-11 : Mise à jour pour RDO 2023
- 2024-01 : Ajout API HAL
